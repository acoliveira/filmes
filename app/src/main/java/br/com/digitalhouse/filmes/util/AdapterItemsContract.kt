package br.com.digitalhouse.filmes.util

interface AdapterItemsContract {
    fun replaceItems(list: List<*>)
}
